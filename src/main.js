import Vue from 'vue'
import App from './App.vue'
import intro from './views/intro'
import table_of_content from './views/table_of_content'
import chapter_one from './views/chapter_one'
import assessment_1 from './views/assessment_1'
import test_two from './views/test_two'
import test_three from './views/test_three'
import test_four from './views/test_four'
import test_five from './views/test_five'
import test_six from './views/test_six'
import chapter_two from './views/chapter_two'
import chapter_three from './views/chapter_three'
import chapter_four from './views/chapter_four'
import chapter_five from './views/chapter_five'
import chapter_six from './views/chapter_six'
import abbreviations from './views/abbreviations'
import login from './views/login'
import home from './views/home'
import VueRouter from 'vue-router'
import Vuex from 'vuex';
import Embed from 'v-video-embed'

import {
  BootstrapVue,
  IconsPlugin,
  ToastPlugin
} from 'bootstrap-vue'

Vue.config.productionTip = false
Vue.use(VueRouter)
Vue.use(BootstrapVue)
Vue.use(IconsPlugin)
Vue.use(ToastPlugin)
Vue.use(Embed);
Vue.use(Vuex)

const routes = [{
  path: '/',
  name: 'home',
  component: home
},
{
  path: '/login',
  name: 'login',
  component: login
},
{
  path: '/introduction',
  name: 'introduction',
  component: intro
},
{
  path: '/assessment_1',
  name: 'assessment_1',
  component: assessment_1
},
{
  path: '/test_two',
  name: 'test_two',
  component: test_two
},
{
  path: '/test_three',
  name: 'test_three',
  component: test_three
},
{
  path: '/test_four',
  name: 'test_four',
  component: test_four
},
{
  path: '/test_five',
  name: 'test_five',
  component: test_five
},
{
  path: '/test_six',
  name: 'test_six',
  component: test_six
},
{
  path: '/chapter_one',
  name: 'chapter_one',
  component: chapter_one
},
{
  path: '/chapter_two',
  name: 'chapter_two',
  component: chapter_two
},
{
  path: '/chapter_three',
  name: 'chapter_three',
  component: chapter_three
},
{
  path: '/chapter_four',
  name: 'chapter_four',
  component: chapter_four
},
{
  path: '/chapter_five',
  name: 'chapter_five',
  component: chapter_five
},
{
  path: '/chapter_six',
  name: 'chapter_six',
  component: chapter_six
},
{
  path: '/abbreviations',
  name: 'abbreviations',
  component: abbreviations
},
{
  path: '/table_of_content',
  name: 'table_of_content',
  component: table_of_content
}
]

export const router = new VueRouter({
  mode: 'history',
  routes
})


export const stores = new Vuex.Store({
  state: {
    // user : {},
    // token : '',
    level: "chapter_two",
    count: 2
  },
  getters: {
    // products: (state) => {
    //   return state.products
    // },
    // level: (state) => {
    //   return state.level
    // }
  },
  plugins: [],
  mutations: {
    //   SET_USER: (state, newValue) => {
    //   state.user = newValue
    // },
    // SET_TOKEN: (state, newValue) => {
    //   state.token = newValue
    // },
    SET_LEVEL: (state, newValue) => {
      state.level = newValue
    },
    SET_COUNT: (state, newValue) => {
      state.count = newValue
    },
  },
  actions: {
    // setUser: ({commit, state}, newValue) => {
    //   commit('SET_USER', newValue)
    //   return state.user
    // },
    // setToken: ({commit, state}, newValue) => {
    //   commit('SET_TOKEN', newValue)
    //   return state.token
    // },
    setLevel: ({ commit, state }, newValue) => {
      commit('SET_LEVEL', newValue)
      return state.level
    },
    setCount: ({ commit, state }, newValue) => {
      commit('SET_COUNT', newValue)
      return state.count
    }
  }
})

Vue.prototype.routes = router

new Vue({
  router,
  store: stores,
  render: h => h(App),
}).$mount('#app')
